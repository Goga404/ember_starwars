import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import ENV from './../../../config/environment';

module('Unit | Controller | main', function(hooks) {
  setupTest(hooks);

  test('data test - ships', function(assert) {

    const controller = this.owner.lookup('controller:main');
    // check the properties before the action is triggered
    assert.equal(
      controller.starShips,
      null,
      'prop starShips initialized'
    );
    assert.equal(
      controller.firstWinCount,
      0,
      'prop firstWinCount initialized'
    );
    assert.equal(
      controller.secondWinCount,
      0,
      'prop secondWinCount initialized'
    );
    assert.equal(
      controller.criteria,
      'crew',
      'prop criteria initialized, "crew" - is default value'
    );
    assert.equal(
      controller.draw,
      false,
      'prop draw initialized'
    );

  });

  // trigger the action on the controller by using the `send` method,
  // passing in any params that our action may be expecting

  test('get starships data', async function(assert) {
    const controller = this.owner.lookup('controller:main');

    // cannot get data from route model() hook (?)
    controller.starShips = await fetch(ENV.API_BASE)
      .then(response => response.json())
      .then(ships => ships.results)

    await controller.getStarShips('', 'getStarShips works!');

    assert.equal(
      Array.isArray(controller.starShips),
      true,
      'starShips updated - got Array'
    );
    assert.equal(
      controller.starShips.length > 1,
      true,
      'starShips array contains at least 2 elements!'
    );

    // check that every select force option has according object property:
    controller.listOfForce.forEach(forceOption => {
      assert.equal(
        controller.starShips[0].hasOwnProperty(forceOption),
        true,
        `Starship data object has required property (to select force) - ${forceOption}`
      );
    })

  });

});
