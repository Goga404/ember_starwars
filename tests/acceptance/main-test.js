import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | main', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting /main', async function(assert) {
    await visit('/main');

    assert.equal(currentURL(), '/main');
    assert.dom('.init_btn').exists();
    assert.dom('.main_btn_wrap select').exists();

    await click('.init_btn');
    assert.dom('.ship_card').exists();
  });
});
