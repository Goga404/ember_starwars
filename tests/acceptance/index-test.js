import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | index', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting /index', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');
    assert.dom(".intro_hdr_wrap").exists();

    await click('.intro_hdr_wrap a');
    assert.equal(currentURL(), '/main');
  });
});
