import { getImageName } from './../../../helpers/card-image';
import { module, test } from 'qunit';

module('Unit | Helper | card-image', function() {
  test('image helper - get image name based on ship.name', function(assert) {
    assert.equal(
      typeof getImageName('test'),
      'string',
      'getImageName() returns string!'
    );
  });
});
