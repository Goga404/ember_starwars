import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | card', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders the Card', async function(assert) {
    await render(hbs`<Card></Card>`);

    assert.dom('.ship_card .card-img-top').exists();
    assert.dom('.ship_card .card-title').exists();
    assert.dom('.ship_card .card-text').exists();
    assert.dom('.ship_card .card_value').exists();
  });
});
