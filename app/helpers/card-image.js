import { helper } from '@ember/component/helper';

export function getImageName([shipName]) {
  if (shipName) {
    shipName = shipName.toLowerCase()

    if (shipName.includes('y-wing')) return 'YWing.png';
    if (shipName.includes('death')) return 'dethstar.png';
    if (shipName.includes('x-wing')) return 'X-Wing.png';
    if (shipName.includes('tie')) return 'tie.png';
    if (shipName.includes('fighter')) return 'Fighter.png';
    if (shipName.includes('millenium')) return 'millenium.png';
    if (shipName.includes('destroyer')) return 'StarDestroyer.png';
  }

  return 'ship.png'
}

export default helper(getImageName);
