import Model, { attr } from '@ember-data/model';

export default class SpaceshipModel extends Model {
  @attr("string") name;
  @attr("string") model;
  @attr("boolean") won;
  @attr("string") starship_class;
  @attr("number") crew;
  @attr("number") hyperdrive_rating;
  @attr("number") cargo_capacity;
  @attr("number") max_atmosphering_speed;
  @attr("number") MGLT;
  @attr("number") cost_in_credits;
}
