import Route from '@ember/routing/route';
import fetch from 'fetch';
import ENV from './../config/environment';

export default class MainRoute extends Route {
  async model(){
    // get spaceship data and store to Ember Data:

    // wait for all url requests
    const shipsData = await fetch(ENV.API_BASE)
      .then(response => response.json())
      .then(ships => ships.results)
      .catch(error => console.error(error))
    // map ships data to add ID and other props
    this.store.push({
      data: shipsData.map( (item, idx) => {
        const newObj = {}
        newObj['attributes'] = item
        newObj['id'] = idx
        newObj['won'] = false
        newObj['type'] = 'spaceship'
        return newObj
      })
    })
  }
}
