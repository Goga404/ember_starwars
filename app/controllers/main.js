import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MainController extends Controller {
  @tracked starShips = null
  @tracked firstWinCount = 0
  @tracked secondWinCount = 0
  @tracked criteria = 'crew'
  @tracked draw = false

  listOfForce = [
    'crew',
    'hyperdrive_rating',
    'cargo_capacity',
    'max_atmosphering_speed',
    'MGLT',
    'cost_in_credits'
  ]

  @action
  setForce(e) {
    this.criteria = e.target.value
  }

  @action
  getStarShips() {
    // check Ember Data contains objects
    if (!this.store.peekAll('spaceship').length){
      console.error("No data provided by Ember Data - check route model() hook")
      return false
    }
    //reset
    this.draw = false

    const generateRandom = max => Math.floor(Math.random() * Math.floor(max))

    // get number of records in Ember Data to generate random records idx's
    const shipsCount = this.store.peekAll('spaceship').length

    // return array of objects (count = Array.length)
    const getRandomShips = count => {
      let result = []
      for (let i=0; i<count; i++){
        result.push(this.store.peekRecord('spaceship', generateRandom(shipsCount)))
      }
      return result
    }

    //array
    this.starShips = getRandomShips(2)

    //comparison criteria should be a Number (Transforms?)
    this.starShips.map(ship => {
      ship[this.criteria] = Number(ship[this.criteria])
      // reset won attr todo - more elegant
      ship.won = false
      return ship
    })

    if (this.starShips[0][this.criteria] > this.starShips[1][this.criteria]){
      this.starShips[0]['won'] = true
      this.firstWinCount++
    } else if (this.starShips[0][this.criteria] < this.starShips[1][this.criteria]) {
      this.starShips[1]['won'] = true
      this.secondWinCount++
    } else { //dead heat
      this.draw = true
    }
  }
}
